<?php

/**
 * @file
 * Exposes environment variables as tokens in Drupal.
 */

/**
 * Implements hook_help().
 */
function token_env_help($path, $arg) {
  switch ($path) {
    case 'admin/help#token_env':
      if (current_path() != 'admin/help/token_env') {
        // Because system_modules() executes hook_help() for each module to 'test'
        // if they will return anything, but not actually display it, we want to
        // return a TRUE value if this is not actually the help page.
        return TRUE;
      } else {
        $output = '<h3>' . t('About') . '</h3>';
        $output .= '<p>' . t('Exposes environment variables as tokens. Only the variables configured will be exposed. Take special care with existing tokens that may stop working as the settings are changed.') . '</p>';
        return $output;
      }
      break;
  }
}

/**
 * Implements hook_permission().
 */
function token_env_permission() {
  return array(
    'administer environment tokens' => array(
      'title' => t('Administer environment tokens'),
      'description' => t('Perform administration tasks for environment tokens.'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_menu().
 */
function token_env_menu() {
  $items['admin/config/system/token-env'] = array(
    'title' => 'Environment tokens',
    'description' => 'Settings to configure the exposed environment variables as tokens to be used in Drupal.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('token_env_settings_form'),
    'access arguments' => array('administer environment tokens'),
    'file' => 'token_env.admin.inc',
  );

  $items['admin/config/system/token-env/list'] = array(
    'title' => 'List',
    'weight' => 10,
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  $items['admin/config/system/token-env/parse'] = array(
    'title' => 'Parsing order',
    'description' => 'Display the environment variables order in PHP.',
    'page callback' => 'token_env_parse',
    'access arguments' => array('administer environment tokens'),
    'file' => 'token_env.admin.inc',
    'weight' => 20,
    'type' => MENU_LOCAL_TASK,
  );

  $items['admin/config/system/token-env/exclude'] = array(
    'title' => 'Exclude',
    'description' => '',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('token_env_exclude_form'),
    'access arguments' => array('administer environment tokens'),
    'file' => 'token_env.admin.inc',
    'weight' => 30,
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Auxiliary function to return all available superglobal variables as they are parsed.
 * It is a recursive function that adds superglobal values following the sequence defined
 * in the $order parameter.
 * 
 * The superglobals must be parsed and composed following the environment variables oeder
 * defined in PHP as there may be values that wil overwrite previous ones.
 * 
 * @param array $superglobal
 *    The array of superglobals to process. Initially an empty array.
 * 
 * @param string $order
 *    String for the variables order from PHP. It is chopped one character on each pass.
 *  
 * @return array
 *    The array with the superglobals with the values defined by the environment variables 
 *    order from PHP.
 */
function _token_env_superglobals($superglobal = array(), $order = NULL) {
  if (empty($order)) {
    return $superglobal;
  } else {
    switch ($order[0]) {
      case 'C':
        $superglobal = array_merge($superglobal, $_COOKIE);
        break;
      case 'E':
        $superglobal = array_merge($superglobal, $_ENV);
        break;
      case 'G':
        $superglobal = array_merge($superglobal, $_GET);
        break;
      case 'P':
        $superglobal = array_merge($superglobal, $_POST);
        break;
      case 'R':
        $superglobal = array_merge($superglobal, $_REQUEST);
        break; 
      case 'S':
        $superglobal = array_merge($superglobal, $_SERVER);
        break;
    }
    return _token_env_superglobals($superglobal,substr($order, 1));
  }
}

/**
 * Auxiliary function to find out if a environment variable is excluded from being
 * used as a toke. If so it will be included in the variable token_env_exclude.
 *
 * @param string $name
 *    Name of the environment variable to match against the ones configured to be expluded.
 * 
 * @return bool
 */
function _token_env_is_excluded($name) {
  $excluded = explode(PHP_EOL, variable_get('token_env_exclude', NULL));
  return in_array($name, $excluded);
}