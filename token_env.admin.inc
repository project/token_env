<?php

/**
 * @file
 * Admin page callbacks for the Environment Token module.
 */

/**
 * Form builder to configure the variables exposed as tokens
 *
 * @ingroup forms
 */
function token_env_settings_form($form, $form_state) {
  $form['token_env_info'] = array(
    '#type' => 'item',
    '#title' => t('Available environment variables'),
    '#markup' => t('Select, amongst the available environment variables, the ones that will be exposed as tokens.<br />
      The tokens will be of the form <em>env:[ENV_VAR_NAME]</em>. Capitalization matters.<br />
      The <a href="/admin/config/system/token-env/exclude">excluded environment variables</a> are not available here.'),
  );
  
  $header = array(
    'env_variable_name' => t('Environment variable'),
    'env_variable_value' => t('Value'),
  );
  $options = array();

  $valid_superglobals = array_diff_key(_token_env_superglobals(array(),ini_get('variables_order')), drupal_map_assoc(array_map('trim',explode(PHP_EOL, variable_get('token_env_exclude', NULL)))));

  foreach ($valid_superglobals as $var_name => $var_value) {
    $options[$var_name] = array(
      'env_variable_name' => $var_name,
      'env_variable_value' => $var_value,
    );
  }

  $orphan_superglobals = array_diff_key(variable_get('token_env_vars', array()),_token_env_superglobals(array(),ini_get('variables_order')));

  if(!empty($orphan_superglobals)){
    $form['token_env_purge'] = array(
      '#type' => 'item',
      '#title' => t('The following will be purged from the database'),
      '#markup' => t('The environment variables %list are saved in the database but they don\'t exist in the current environment variables configuration.<br />
      They will be deleted when saving changes to this form.', array('%list' => implode(', ', $orphan_superglobals))),
    );
  }
  
  $form['token_env_vars'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No environment variables found.'),
    '#default_value' => array_map('is_string',variable_get('token_env_vars', array())),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

drupal_set_message(t('Beware of the environment variables with relative values. Their value will change depending on the context where they are used.'),'warning',FALSE);

  return $form;
}

/**
 * Form validation handler for token_env_settings_form().
 *
 * @ingroup forms
 */
function token_env_settings_form_validate() {}

/**
 * Form submission handler for token_env_settings_form().
 *
 * @ingroup forms
 */
function token_env_settings_form_submit($form,&$form_state) {
  $results = array_filter($form_state['values']['token_env_vars']);
  variable_set('token_env_vars', $results);
  drupal_set_message(t('Variables updated.'));

  //Clear tokens caches
  token_clear_cache();
}

/**
 * Page builder to display the environment variables order in PHP
 * 
 * @return array
 *    Renderable array to display the HTML for the page.
 */
function token_env_parse() {
  $page['token_env_order'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Environment variables parsing order'),
    '#description' => t('Parsing order of enviornment variables as defined in the PHP configuration. See <a href="http://php.net/manual/en/ini.core.php#ini.variables-order">the PHP manual</a> for more reference.'),
    '#collapsible' => FALSE, 
    '#collapsed' => FALSE,
  );

  $page['token_env_order']['variables_order'] = array(
    '#type' => 'item',
    '#title' => t('PHP variables order'),
    '#markup' => ini_get('variables_order'),
    '#description' => t('This is the order set in the server to parse the superglobal variables: EGPCS (Environment, Get, Post, Cookie, and Server).<br />
    If a letter is not present, the corresponding superglobal array will not be populated. For example, if variables_order is set to "SP" then PHP will create the superglobals $_SERVER and $_POST, but not create $_ENV, $_GET, and $_COOKIE. Setting to "" means no superglobals will be set.
    <p><em>Note that in both the CGI and FastCGI SAPIs, $_SERVER is also populated by values from the environment; S is always equivalent to ES regardless of the placement of E elsewhere in this directive.</em></p>'),
  );
  return $page;
}

/**
 * Form builder to configure the environment variables excluded from being available as tokens
 *
 * @ingroup forms
 */
function token_env_exclude_form() {
  $form['token_env_exclude'] = array(
    '#title' => t('Excluded environment variables'),
    '#type' => 'textarea',
    '#description' => t('Add here all the environment variables that will be excluded from being available as tokens. Write each variable in a new line. The names are case-sensitive.<br />
      Typically unsafe variables that you may not want to use as tokens are HTTP request values, as they could be tampered with.'),
    '#default_value' => variable_get('token_env_exclude', NULL),
  );

  return system_settings_form($form);
}