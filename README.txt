CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting

INTRODUCTION
------------

The Environment Tokens module exposes environment variables as tokens in a 
Drupal site.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/token_env

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/token_env

REQUIREMENTS
------------

This module requires the following modules:

 * Token (https://www.drupal.org/project/token)
 * Drupal 7.40 or higher

INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 
 * Configure user permissions in Administration » People » Permissions:

   - Administer environment tokens

     Users in roles with the "Administer environment tokens" permission will 
     be able to configure the module and to choose the environment variables
     that will be exposed as tokens.

 * Customize the Environment Tokens settings in Administration » Configuration »
   System » Environment Tokens.

TROUBLESHOOTING
---------------

 * If the tokens do not have the expected values:

   - Check that they are not context dependent. Context environment variables 
     will have different value depending on where they are called from.

   - Check the order of the environment variables parsing on PHP by visiting
     Administration » Configuration » System » Environment Tokens » Parsing order.

