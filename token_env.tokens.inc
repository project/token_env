<?php

/**
 * @file
 * Token integration for the Environment Tokens module.
 */

/**
 * Implements hook_token_info().
 */
function token_env_token_info() {
  $variables = variable_get('token_env_vars', array());

  if(!empty($variables)) {
    $info['types']['env'] = array(
      'name' => t('Environment'),
      'description' => t('Environment tokens.'),
    );

    $valid_superglobals = array_diff_key(_token_env_superglobals(array(),ini_get('variables_order')), drupal_map_assoc(array_map('trim',explode(PHP_EOL, variable_get('token_env_exclude', NULL)))));

    foreach ($variables as $var_name) {
      $info['tokens']['env'][$var_name] = array(
        'name' => $var_name,
        'description' => $valid_superglobals[$var_name], //replace with an abstraction
      );
    }
    return $info;
  }
}

/**
 * Implements hook_tokens().
 */
function token_env_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  $sanitize = !empty($options['sanitize']);

  $valid_superglobals = array_diff_key(_token_env_superglobals(array(),ini_get('variables_order')), drupal_map_assoc(array_map('trim',explode(PHP_EOL, variable_get('token_env_exclude', NULL)))));

  if ($type == 'env' AND variable_get('token_env_vars', FALSE)) {
    foreach ($tokens as $name => $original) {
        if (array_key_exists($name, $valid_superglobals)) {
          $replacements[$original] = $valid_superglobals[$name];	
        }
    }
  }
  return $replacements;
}